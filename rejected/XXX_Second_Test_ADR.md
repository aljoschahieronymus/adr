---
id: 1234
title: Schnittstelle zwischen UP und MP
short_name: UP2MP
date: 29.05.2020
revision: 2.1
author:
    - Max Muster (max@example.com)
    - Elfriede Example (e.example@example.com)
stakeholder:
    - UP
    - MP
    - IPA
    - SAG
status: draft
---

 # Titel
    
<Abstract>

# Kontext
Zweiter ADR, der nicht in accepted übernommen werden soll

# Entscheidung

# Konsequenz

# Diskusion